import React from "react";
import { Link } from "react-router-dom";
import './post.css';

export default class Post extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userData: {},
            showComment: Array(101).fill(false),
        }
    }

    componentDidMount() {
        const usersData = this.props.data.users;
        const usersName = usersData.reduce((acc, cur) => {
            acc[cur.id] = cur.username;
            return acc;
        }, {});

        this.setState({
            userData: usersName,
        })
    }

    showComment(id) {
        const arr = this.state.showComment;
        arr[id] = !arr[id]
        this.setState({
            showComment: arr
        })
    }

    render() {
        return (
            <div>
                {this.props.data.posts.map((post) => (
                    <div className="container" key={post.id}>
                        <div className="container-1">
                            <div className="container-child-1">
                                {/* <span ></span> */}
                                <Link to={{
                                        pathname: `/user/${post.userId}`,
                                    }} className="link-tag fa fa-user-circle-o"></Link>
                            </div>
                            <div className="container-child-2">
                                <div className="user-container">
                                    <Link to={{
                                        pathname: `/user/${post.userId}`,
                                    }} >@{this.state.userData[post.userId]}</Link>
                                    <img className="post-verified" src={require('./img/verified.png')} alt="Verfied Badge" />
                                </div>
                                <Link  to={{
                                    pathname: `/post/${post.id}`
                                }} className="post-container link-tag">
                                    <p className="title">{post.title}</p>
                                    <p className="para">{post.body}</p>
                                </Link>
                                <div className="interactive">
                                    <i className="fa fa-heart"></i>
                                    <i className="fa fa-comment" aria-hidden="true" onClick={() => { this.showComment(post.id) }}></i>
                                </div>
                            </div>
                        </div>
                        {this.state.showComment[post.id] ? (
                            <div>
                                {this.props.data.comments.map((comment) => {
                                    if (comment.postId === post.id) {
                                        return (
                                            <div className="container">
                                            <div className="container-1" key={comment.id}>
                                                <div className="container-child-1">
                                                    <span className="fa fa-user-circle-o"></span>
                                                </div>
                                                <div className="container-child-2">
                                                    <div className="user">
                                                        <p className="name">{comment.name}</p>
                                                        <img className="verified" src={require('./img/verified.png')} alt="Verfied Badge" />
                                                        <p className="email">{comment.email}</p>
                                                    </div>
                                                    <div className="body">
                                                        <p>{comment.body}</p>
                                                    </div>
                                                    <div className="interactive">
                                                        <i className="fa fa-heart"></i>
                                                        <i className="fa fa-comment" aria-hidden="true" onClick={() => { this.showComment(post.id) }}></i>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        );
                                    } else {
                                        return undefined;
                                    }
                                })}
                            </div>
                        ) : undefined}
                    </div>
                ))
                }
            </div>
        )
    }
}