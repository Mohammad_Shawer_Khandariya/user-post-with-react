import React from "react";
import axios from "axios";
import Page from "./Pagenotfound";

export default class Userprofile extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userProfile: [],
            hasFetched: false,
            notFound: false,
        }
    }

    async fetchUser(id) {
        try {
            const userResult = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
            const userData = userResult.data;
            console.log(id, userResult);
            this.setState({
                userProfile: userData,
                hasFetched: true,
            })
        } catch (err) {
            this.setState({
                notFound: true,
            })
        }
    }

    componentDidMount() {
        this.fetchUser(this.props.match.params.id)
    }

    render() {
        console.log(this.state.userData);
        return (
            <div>
                {(this.state.notFound ?
                    <Page />
                    :
                    <div className="user">
                        {
                            (this.state.hasFetched) ?
                                <div>
                                    <h1>{this.state.userProfile.name}</h1>
                                    <div className="user-profile">
                                        <div className="bg-img">
                                            {/* <img src={require('./img/bg.jpg')} alt="Background Image" /> */}
                                            <span className="fa fa-user-circle-o"></span>
                                        </div>
                                        <div className="profile-info">
                                            <h3>{this.state.userProfile.name}</h3>
                                            <p>{this.state.userProfile.email}</p>
                                        </div>
                                        <div className="profile-bio">
                                            {this.state.userProfile.company.catchPhrase}
                                        </div>
                                        <div className="profile-details">
                                            <div className="user-address">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                <span>{this.state.userProfile.address.city}</span>
                                            </div>
                                            <div className="user-contact">
                                                <i class="fa fa-link" aria-hidden="true"></i>
                                                <p className="link-style">{this.state.userProfile.website}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                :
                                <h1>Loading...</h1>
                        }
                    </div>
                )}
            </div>
        )
    }
}