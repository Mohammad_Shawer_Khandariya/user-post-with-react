import React from "react";
import axios from "axios";
import Page from "./Pagenotfound";
import "./Userpost.css";

export default class Userpost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: [],
            post: [],
            comment: [],
            hasPostFetched: false,
            showComment: false,
            notFound: false,
        }
    }

    async fetchPost(id) {
        try {
            const postResult = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`)
            const postData = postResult.data;
            const commentResult = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}/comments`)
            const commentData = commentResult.data;
            const userResult = await axios.get(`https://jsonplaceholder.typicode.com/users/${postData.userId}`)
            const userData = userResult.data;
            this.setState({
                user: userData,
                post: postData,
                comment: commentData,
                hasPostFetched: true,
            })
        } catch (err) {
            this.setState({
                notFound: true,
            })
        }
    }

    getComment() {
        const commentStatus = this.state.showComment;
        this.setState({
            showComment: !commentStatus,
        })
    }

    componentDidMount() {
        this.fetchPost(this.props.match.params.id)
    }

    render() {
        return (
            <div>
                {
                    (this.state.notFound) ?
                        <Page />
                        :
                        <div>
                            {(this.state.hasPostFetched) ?
                                <div>
                                    {/* <h1>Userpost</h1> */}
                                    <div>
                                        <div className="user-info">
                                        <span className="fa fa-user-circle-o"></span>
                                        <div>
                                            <h4>{this.state.user.name}</h4>
                                            <span>{this.state.user.email}</span>
                                        </div>
                                        </div>
                                        <div className="user-post-content">
                                        <p>{this.state.post.body}</p>
                                        <div className="interactive">
                                            <i className="fa fa-heart"></i>
                                            <i className="fa fa-comment" aria-hidden="true" onClick={() => { this.getComment() }}></i>
                                        </div>
                                        </div>
                                    </div>
                                    {(this.state.showComment) ?
                                        <div>
                                            {this.state.comment.map((comment) => {
                                                return (
                                                    <div className="container">
                                                        <div className="container-1" key={comment.id}>
                                                            <div className="container-child-1">
                                                                <span className="fa fa-user-circle-o"></span>
                                                            </div>
                                                            <div className="container-child-2">
                                                                <div className="user">
                                                                    <p className="name">{comment.name}</p>
                                                                    <img className="verified" src={require('./img/verified.png')} alt="Verfied Badge" />
                                                                    <p className="email">{comment.email}</p>
                                                                </div>
                                                                <div className="body">
                                                                    <p>{comment.body}</p>
                                                                </div>
                                                                <div className="interactive">
                                                                    <i className="fa fa-heart"></i>
                                                                    <i className="fa fa-comment" aria-hidden="true" ></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                            }
                                        </div>
                                        :
                                        undefined}
                                </div>
                                :
                                <p>Loading...</p>}
                        </div>
                }
            </div>
        )
    }
}