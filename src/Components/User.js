import React from 'react';
import axios from 'axios';
import Post from './Post';

export default class User extends React.Component {

  constructor() {
    super();
    this.state = {
      users: [],
      posts: [],
      comments: [],
      isData: false,
    }
  }


  async fetchData() {
    try {
      const userResponse = await axios.get('https://jsonplaceholder.typicode.com/users');
      const users = await userResponse.data;
      const postResponse = await axios.get('https://jsonplaceholder.typicode.com/posts');
      const posts = await postResponse.data;
      const commentResponse = await axios.get('https://jsonplaceholder.typicode.com/comments');
      const comments = await commentResponse.data;
      this.setData(users, posts, comments);
    } catch (error) {
      console.error(error);
    }
  }

  setData(user, post, comment) {
    const hasData = this.state.isData;
    this.setState({
      posts: post,
      users: user,
      comments: comment,
      isData: !hasData,
    })
  }

  componentDidMount() {
    this.fetchData();
  }

  render() {
    return (
      <div className="App">
        {this.state.isData === true ? 
      <Post data={this.state}/> : undefined}
      </div>
    );
  }
}