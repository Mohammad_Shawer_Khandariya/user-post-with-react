import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import User from './Components/User';
import Userprofile from './Components/Userprofile';
import Userpost from './Components/Userpost';
import './App.css';

export default class App extends React.Component {

  render() {
    return (
      <Router>
        {/* <div className="App"> */}
        <Switch>
          <Route exact path="/" component={User}/>
          <Route exact path="/user/:id" component={Userprofile} />
          <Route exact path="/post/:id" component={Userpost} />
        </Switch>
        {/* </div> */}
      </Router>
    );
  }
}